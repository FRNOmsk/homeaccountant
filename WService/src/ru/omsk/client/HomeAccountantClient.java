package ru.omsk.client;

import java.net.URL;
import java.net.MalformedURLException;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import ru.omsk.ws.HomeAccountant;

public class HomeAccountantClient {

	public static void main(String[] args) throws MalformedURLException {
		URL url = new URL("http://localhost:1977/wss/HomeAccountant?wsdl");
		QName qname = new QName("http://ws.omsk.ru/", "HomeAccountantImplService");
		Service service = Service.create(url, qname);
		HomeAccountant homeacc = service.getPort(HomeAccountant.class);
		System.out.println(homeacc.getMenu());
	}

}
