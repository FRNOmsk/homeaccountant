package ru.omsk.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


@SOAPBinding(style = SOAPBinding.Style.RPC)
@WebService

public interface HomeAccountant {
	@WebMethod
	public String getMenu();
}
