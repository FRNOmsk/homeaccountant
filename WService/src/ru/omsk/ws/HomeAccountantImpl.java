package ru.omsk.ws;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.omsk.ws.HomeAccountant")

public class HomeAccountantImpl implements HomeAccountant {
	@Override
	public String getMenu() {
		return "File, Edit, Open, Exit";
	}

}
